package toantmt.net.rxjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import rx.Observable;
import rx.Subscriber;
import timber.log.Timber;


public class MainActivity extends AppCompatActivity {

    private String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // create Observable
        createObservable();

        transfromingObservable();



    }

    private void createObservable(){
        CreateObservable observable = new CreateObservable();
        //observable.onCreate();
        //observable.onDefer();
        //observable.onEmpty();
        //observable.onNever();
        //observable.onThrow();
        //observable.onFrom();
        //observable.onInterval();
        //observable.onJust();
        //observable.onRange();
        //observable.onTimber();
    }

    private void transfromingObservable(){
        TransfromingObservable observable = new TransfromingObservable();
        observable.onBuffer();
    }


}
