package toantmt.net.rxjava;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Action1;
import timber.log.Timber;

/**
 * Created by USER on 3/29/2017.
 */

public class TransfromingObservable {

    public void onBuffer(){
        Observable observable = Observable.create(subscriber -> {
            subscriber.onNext(1);
            subscriber.onNext(2);
            subscriber.onNext(3);
        }).buffer(2, TimeUnit.SECONDS);

        observable.subscribe(new Action1() {
            @Override
            public void call(Object o) {
                Timber.d("onBuffer : " + o);
            }
        });
    }

    public void onFlatMap(){

    }

}
