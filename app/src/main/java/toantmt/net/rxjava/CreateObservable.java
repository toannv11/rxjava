package toantmt.net.rxjava;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by USER on 3/29/2017.
 */

public class CreateObservable {

    private String message = "";

    public void onCreate(){
        Observable.create(subscriber -> {
            subscriber.onNext(1);
            subscriber.onNext(2);
            subscriber.onCompleted();
            // subscriber.onNext(4);
        }).subscribe(message -> {
            Timber.i("onCreate Observable: " + message);
        }, throwable -> {
            Timber.e(throwable, "onCreate Observable: ");
        });
    }

    public void onDefer(){
        message = "Hello Defer";
        Observable<String> observable = Observable.defer(() -> {
            return Observable.just(message);
        });
        message += " Lastest";

        observable.subscribe(new Subscriber<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                Timber.i("onNext : " + s);
            }
        });
    }

    public void onEmpty(){
        // create an Observable that emits no item but terminates normally.
        Observable observable = Observable.empty();
        observable.subscribe(new Subscriber() {
            @Override
            public void onCompleted() {
                Timber.d("onEmpty : onCompleted ");
            }

            @Override
            public void onError(Throwable e) {
                Timber.d("onEmpty : onError ");
            }

            @Override
            public void onNext(Object o) {
                Timber.d("onEmpty : onNext ");
            }
        });
    }

    public void onNever(){
        // create an Observable that emits no item but does not terminate
        Observable observable = Observable.never();
        observable.subscribe(new Subscriber() {
            @Override
            public void onCompleted() {
                Timber.d("onNever : onCompleted ");
            }

            @Override
            public void onError(Throwable e) {
                Timber.d("onNever : onError ");
            }

            @Override
            public void onNext(Object o) {
                Timber.d("onNever : onNext ");
            }
        });
    }

    public void onThrow(){
        // create an Observable that emits no item but terminates with an error
        Observable observable = Observable.error(new Exception("error in onThrow"));
        observable.subscribe(new Subscriber() {
            @Override
            public void onCompleted() {
                Timber.d("onNever : onCompleted ");
            }

            @Override
            public void onError(Throwable e) {
                Timber.d("onNever : onError " + e.toString());
            }

            @Override
            public void onNext(Object o) {
                Timber.d("onNever : onNext ");
            }
        });
    }

    public void onFrom(){
        String[] strArr = new String[]{"Nguyen", "Van", "Toan"};
        Observable observable = Observable.from(strArr);
        observable.subscribe(message ->{
            Timber.d("onFrom : " + message);
        });
    }

    public void onInterval(){
        Observable observable = Observable.interval(1000, TimeUnit.MILLISECONDS).map(message -> {
            return "onInterval";
        });
        observable.subscribe(message ->{
            Timber.d(message.toString());
        });
    }

    public void onJust(){
        Integer[] integers = {1,2,3};
        Observable observable = Observable.just(integers, 1, 2, 3);
        observable.subscribe(message -> {
            if (message instanceof int[]) {
                Timber.d("onJust " + Arrays.toString((int[])message));
            }else {
                Timber.d("onJust " + message);
            }
        });
    }

    public void onRange(){
        Observable observable = Observable.range(1, 10)
                .map(message -> {
                    return "Hello " + message.intValue();
                });
        observable.subscribe(message -> {
            Timber.d("onRange : " + message);
        });
    }

    public void onRepeat(){

    }

    public void onStart(){

    }

    public void onTimber() {
        Timber.d("onTimber : start ");
        Observable observable = Observable.timer(10, TimeUnit.SECONDS)
                .map(message -> {return "Hello ";});
        observable.subscribe(message -> {
            Timber.d(message +  " onTimber : end ");
        });
    }



}
