package toantmt.net.rxjava;

import android.app.Application;

import timber.log.Timber;


public class RxJavaApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }
}
